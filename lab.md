# Penetration Testing-Lab

#### Penetration Testing
By definition, penetration testing is a method for testing a web application, network, or computer system to identify security vulnerabilities that could be exploited. The primary objective for security as a whole is to prevent unauthorized parties from accessing, changing, or exploiting a network or system. It aims to do what a bad actor would do.

Consider a Pen Test an authorized simulation of a real-world attack on a system, application, or network to evaluate the security of the system. The goal is to figure out whether a target is susceptible to an attack. Testing can determine if the current defense systems are sufficient, and if not, which defenses were defeated.

These tests are designed to target either known vulnerabilities or common patterns which occur across applications — finding not only software defects but also weaknesses in network configurations.


![image](images/PenetrationTestProcess.png)


#### Steps in Penetration Testing

Intelligence gathering and due diligence analysis are crucial steps to prepare for penetration testing. During this initial phase, due diligence analysis will assist the ethical hackers or cybersecurity personnel who are tasked with testing and fixing your system.  This preparation work will allow an organization yo learn how the environment of the system functions and the gathers as much information as possible about the system before beginning. This phase will usually uncover surface-level vulnerabilities.

This initial step includes a scan of:

- Your local and wireless network
- Pertinent applications, especially mission critical applications such as accounting and HR
- Website(s)
- Cloud-based systems
- Employees
- Physical hardware facilities


After gathering intelligence, cybersecurity professionals move on to threat modeling.

Threat modeling is a structured representation of the information that affects system security. Security teams use this type of model to treat every application or feature as if it were a direct safety.

Threat modeling captures, organizes, and analyzes the bulk of intelligence gathered in the previous phase of preparation for a penetration test. It then makes informed decisions about cybersecurity while prioritizing a comprehensive list of security improvements including concepts, requirements, design, and rapid implementation.

Threat modeling is a process of its own, and can be summed up by asking the following four questions:

- What are we working on?
- What can go wrong with what we’re working on?
- What can we do to ensure that doesn’t happen?
- Did we completely eradicate the problem?



There is no single, right way to investigate vulnerabilities in a system. But combinations of these questions can go a long way toward finding solutions.

During threat modeling, cybersecurity professionals will define and identify the scope of the the assessment, what are the threat agents, what existing countermeasures have already taken place, exploitable vulnerabilities, prioritized risks, and identify possible countermeasures.


#### Types of Penetration Testing
Following intelligence gathering and threat modeling, the next step is the actual penetration test. There are various penetration testing methodologies and it is important to test for as many potential weaknesses throughout your system and network as possible. Conducting multiple tests can reveal more vulnerabilities and provide your security and IT teams with more opportunities to address and eliminate security threats.

#### Network Penetration Testing and Exploitation

This type of test includes both internal and external network exploitation testing through the emulation of hacker techniques that penetrate a system’s network defenses. Once the network has been compromised, the tester can potentially gain access to the internal security credentials of an organization and its operation. Testing of a network includes identifying:

- Threat Modeling
- Router and proxy server testing
- IPS and DPS evasion
- Vulnerability Scanning & Analysis
- Firewall bypassing
- Open port scanning
- SSH security attacks



Network testing is more in-depth than standard penetration testing and locates vulnerabilities that basic scans may not find, all to create a safer overall network.

#### Web Application Tests

Web application security tests look for server-side, web application vulnerabilities. The penetration test is designed to evaluate the potential risks associated with these vulnerabilities through web applications, web services, mobile applications, and secure code review. The most commonly reviewed applications are web apps, languages, APIs, connections, frameworks, systems, and mobile apps.

#### Client Side or Website & Wireless Network

This testing is predominately, a set of hardware tests. Wireless networks and website tests inspect relevant devices and infrastructures for vulnerabilities that may lead to compromises and exploits to the wireless network. For example, many WiFi networks are vulnerable to hacking through their WPA2 protocols. This WPA2 exploit has the potential to reveal all encrypted information including credit card numbers, passwords, chat messages, emails, and images. Of course, this can lead to a manipulation of data is  and can lead to ransomware or malware attacks that could threaten your entire system.

To prevent wireless network hacking, check for the following during pen testing:

- web server misconfiguration including the use of default passwords
- malware and Distributed Denial of Service (DDos) attacks
- SQL Injections
- MAC address spoofing
- media player or other content creation software vulnerabilities
- cross-site scripting
- unauthorized hotspots and access points
- wireless network traffic
- encryption protocols


#### Social Engineering Attacks

Social Engineering Attack tests search for vulnerabilities based on an organization employees.  In this case, penetration testing must be designed to mimic real-world situations that employees could run into without realizing they’re being exploited.  Specific topics such as eavesdropping, tailgating, or phishing attacks, posing as employees or posing as vendors/contractors are common testing practices.  Bad actors typically possess social engineering skills and can influence employees to create access to systems or sensitive customer data. When used in conjunction with other physical tests, social engineering testing can help to develop a culture of security throughout an organization.

#### Physical Testing

Physical penetration testing prevents attacks by securing the physical site from unauthorized personnel. Physical penetration tests focus on attempts to gain access to facilities and hardware through RFID systems, door entry systems and keypads, employee or vendor impersonation, and evasion of motion and light sensors. Physical tests are often used in combination with social engineering such as manipulation and deceit of facility employees to gain system access.


Web Application Penetration Testing Tools
Web Application Penetration Testing is done by simulating unauthorized attacks internally or externally to get access to sensitive data.  A web penetration helps security professionals to find out where the vulnerabilities are that a hacker can take advantage of to access your data, find the security of you email servers and, in general get to know how secure your web hosting site and server are.

There are numerous scanning tools available on the market. Here are a few.

#### netsparker

https://www.netsparker.com/statics/img/youtube-covers/Proof-Based-youtube-cover.jpg

#### Kiuwan

https://youtu.be/nJhNV1vhaLE

#### Vega

https://subgraph.com/vega/

The Vega PEN Test tool is a widely used tool that is open source and provides a GUI interface for both Windows, MacOS and Linux.  Like other PEN Test scanners, Vega can also be run from a command line environment.

Step 1: Before you download the Vega tool make sure you have the current version of Java Runtime Engine (JRE) installed.

Step 2:  Start up the Vega tool. Once open, click the Start New Scan icon in the upper left hand corner.

Step 3: Type in the web address. And then click the Next button.

Step 4:  Vega allows you to select the various modules you want to enable for your scan.

Step 5:  Once you have selected the modules click the Finish button.

Step 6:  Once the scan is complete you will see an alert summary of found issues.

Step 7:  Under the Website View, click the Expand All to see the web pages scanned.  Under Scan Alerts click the Expand All to view the alert messages.

Step 8:  To view the Vega Console logs, click the Console (Vega Log) icon in the lower left corner. This console will show you a log of all of the scanning requests that Vega makes.  You can also open the Console during the scanning process to view the scan log in real time.

Step 9:  To see a report of Requests and Responses in site code context, click the Requests icon right next to the Console icon.  This will show you the Requests Vega made and the Responses.  Errors are highlight in context.

https://youtu.be/tVOwbnNjo8c