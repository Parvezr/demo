<img src="./images/IDSNlogo.png" width="200" height="200"/>

### In this lab you will explore Watson Studio

Objective for Exercise:
* How to create a Watson Studio service instance
* How to create a project in Watson Studio
* How to create project on Watson Studio
* How to load a notebook in Watson Studio.

*If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2*

**Exercise** - Create a project on Watson Studio

**Step 1:** For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

[Click here](https://cocl.us/EdX_Python_101_Pandas)

You will see the screen in the figure below. Click the icon in the top left corner:

<img src="./images/01_Watson_Studio_Landing_page.png" width="600"/>

Then click **Watson**, as shown below:

<img src="./images/02_Watson_Landing_page_menu.png" width="600"/>

You are in the Watson landing page. Click on the **Watson Services** menu.

<img src="./images/03_Watson_landing_page.png" width="600"/>

Then click **Browse Services** and on the page scroll down and choose **Watson Studio**

<img src="./images/04_Browse_Service.png" width="600"/>

Scroll down and select **Watson Studio - Lite.**
To create a Watson service using the Lite plan, click **Create**.

<img src="./images/05_Watson_Studio_Lite_sign_up.png" width="600"/>

Now click **Get Started.**

<img src="./images/06_Watson_Studio.png" width="600"/>

**Step 2:** For Existing Users (who already have Watson Service):
Go to the IBM Cloud Dashboard and click Services.

<img src="./images/07_Dashboard_with_Service.png" width="600"/>

When you click on Services, all your existing services will be shown in the list. Click the Watson Studio service you created:

<img src="./images/08_Resource_List.png" width="600"/>

**Step 3:** Creating a Project
Now you have to Create a project.

Click on **Create a project**

<img src="./images/09_Create_project.png" width="600"/>

On the Create a project page, click **Create an empty project**

<img src="./images/10_Create_Empty_Project_details.png" width="600"/>

Provide a **Project Name** and **Description**, as shown below:

<img src="./images/11_New_Project_details.png" width="600"/>

You must also create storage for the project.
Click **Add**

<img src="./images/12_New_Project_details_Add_Storage.png" width="600"/>

On the Cloud Object Storage page, scroll down and then click **Create**.

<img src="./images/13_Select_storage.png" width="600"/>

In the Confirm Creation box, click **Confirm.**

<img src="./images/14_Confirm_Creation.png" width="600"/>

On the New project page, note that the storage has been added, and then click Create.

<img src="./images/15_Create_Project_Storage_Confirmed.png" width="600"/>

**Step 4:** Adding a Notebook to the Project:
You need to add a Notebook to your project. Click Add to project.

<img src="./images/16_Add_to_Project.png" width="600"/>

In the list of asset types, click Notebook:

<img src="./images/17_Add_notebook.png" width="600"/>

On the New Notebook page, enter a name for the notebook, and then click **From URL**.
Copy this link:
https://cocl.us/PY0101EN43_EDX

Paste it into the **Notebook URL** box, and then click **Create Notebook**.

<img src="./images/18_Load_notebook.png" width="600"/>

You will see this Notebook:

<img src="./images/19_Notebook_loaded.png" width="600"/>

Once you complete your notebook you will have to share it. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all content excluding sensitive code cells. Share it in the Social Network platform, you desire.

<img src="./images/20_Share_Notebook.png" width="600"/>

## Author(s)
<h4> Romeo <h4/>

### Other Contributor(s) 
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-25 | 0.1 | Lavanya | Initial version created |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

